<?php
/**
 * @file
 * Admin page and form callbacks for the Purecloud module.
 */

/**
 * Callback for the Purecloud admin settings form.
 */
function purecloud_settings_form($form, &$form_state) {
  $form['purecloud_api_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#description' => t('The OAuth client ID for this application.'),
    '#default_value' => variable_get('purecloud_api_client_id'),
    '#required' => TRUE,
  );
  $form['purecloud_api_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#description' => t('The OAuth client secret for this application.'),
    '#default_value' => variable_get('purecloud_api_client_secret'),
    '#required' => TRUE,
  );
  $form['purecloud_api_oauth_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth endpoint'),
    '#description' => t('The OAuth endpoint for this application.'),
    '#default_value' => variable_get('purecloud_api_oauth_endpoint'),
    '#required' => TRUE,
  );
  $form['purecloud_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#description' => t('The URL to use for the API.'),
    '#default_value' => variable_get('purecloud_api_url'),
    '#required' => TRUE,
  );
  $form['purecloud_record_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Record lifetime'),
    '#description' => t('Number days PureCloud entities should be kept in Drupal.'),
    '#default_value' => variable_get('purecloud_record_lifetime', 60),
    '#required' => TRUE,
  );
  $form['purecloud_cron_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron interval'),
    '#description' => t('How frequently cron runs (hours).'),
    '#default_value' => variable_get('purecloud_cron_interval', 1),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}