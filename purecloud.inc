<?php
/**
 * @file
 * Helpers for the PureCloud module.
 */

/**
 * Fetch settings for the PureCloud API.
 *
 * @return array
 * An array containing all PureCloud API settings.
 */
function purecloud_api_settings() {
  return array(
    'oauth_endpoint' => variable_get('purecloud_api_oauth_endpoint'),
    'client_id' => variable_get('purecloud_api_client_id'),
    'client_secret' => variable_get('purecloud_api_client_secret'),
    'api_url' => variable_get('purecloud_api_url'),
  );
}

/**
 * Load dependencies for the PureCloud API.
 */
function purecloud_load_api_dependencies() {
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIAuthTokenInterface');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIRequest');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIAuthToken');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPI');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIQuery');
  module_load_include('php', 'purecloud', 'plugins/api/PureCloudAPIAnalyticsQuery');
  module_load_include('php', 'purecloud', 'plugins/api/PureCloudAPIUsersQuery');
  module_load_include('php', 'purecloud', 'plugins/api/PureCloudAPIRoutingQuery');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIPagerIterator');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIEntityHandleInterface');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIEntityHandle');
  module_load_include('php', 'purecloud', 'plugins/entity/PureCloudAPIConversationHandle');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIConversationFilterIterator');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIConversationIterator');
  module_load_include('php', 'purecloud', 'includes/PureCloudConversationFactory');
  module_load_include('php', 'purecloud', 'includes/PureCloudAPIFactory');
}

/**
 * Import conversations.
 *
 * @param string $start
 * A PHP Date string for start time, see PHP DateTime for allowed formats.
 * @param string $end
 * A PHP Date string for end time, see PHP DateTime for allowed formats.
 *
 * @return array
 * The created entities.
 */
function purecloud_import_conversation_range($start, $end) {
  purecloud_load_api_dependencies();
  $settings = purecloud_api_settings();
  $records = Drupal\purecloud\PureCloudConversationFactory::conversation($settings, $start, $end);
  return $records;
}

