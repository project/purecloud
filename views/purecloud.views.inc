<?php
/**
 * @file
 * Views handler data for the PureCloud module
 */

/**
 * Implements hook_views_data_alter().
 */
function purecloud_views_data_alter(&$data) {
	$data['purecloud_conversation']['start'] = array(
    'title' => t('Call date'),
    'help' => t('The date of the conversation.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'is date' => TRUE,
    ),
  );
  $data['purecloud_conversation']['duration'] = array(
    'title' => t('Total call duration'),
    'help' => t('The duration of the conversation.'),
    'field' => array(
      'handler' => 'purecloud_views_handler_field_time_interval',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['purecloud_conversation']['interact'] = array(
    'title' => t('Agent interaction duration'),
    'help' => t('The duration of the conversation where the agent was interacting with the customer.'),
    'field' => array(
      'handler' => 'purecloud_views_handler_field_time_interval',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['purecloud_conversation']['hold'] = array(
    'title' => t('Agent hold time'),
    'help' => t('The duration of time that the caller was on hold.'),
    'field' => array(
      'handler' => 'purecloud_views_handler_field_time_interval',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['purecloud_agent']['table']['group']  = t('PureCloud Conversation');
  $data['purecloud_agent']['table']['join'] = array(
    'purecloud_conversation' => array(
      'left_field' => 'agent',
      'field' => 'agent',
    ),
  );
  $data['purecloud_agent']['name'] = array(
    'title' => t('Agent name'),
    'help' => t('Full name of the agent.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  $data['purecloud_queue']['table']['group']  = t('PureCloud Conversation');
  $data['purecloud_queue']['table']['join'] = array(
    'purecloud_conversation' => array(
      'left_field' => 'queue',
      'field' => 'queue',
    ),
  );
  $data['purecloud_queue']['name'] = array(
    'title' => t('Queue name'),
    'help' => t('Full name of the queue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
}

