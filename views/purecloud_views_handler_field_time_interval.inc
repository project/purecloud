<?php

/**
 * @file
 * Definition of purecloud_views_handler_field_time_interval.
 */

/**
 * A handler to provide proper displays for time intervals.
 *
 * @ingroup views_field_handlers
 */
class purecloud_views_handler_field_time_interval extends views_handler_field {

  /**
   * Render handler for the field
   */
  function render($values) {
    $value = $values->{$this->field_alias};
    return gmdate("G:i:s", $value);
  }

}

