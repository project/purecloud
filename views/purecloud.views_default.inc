<?php
/**
 * @file
 * Default views for the PureCloud module.
 */

/**
 * Implements hook_views_default_views().
 */
function purecloud_views_default_views() {
  $view = new view();
  $view->name = 'purecloud_conversations';
  $view->description = 'Filterable and exportable conversations report of data pulled in from the PureCloud API.';
  $view->tag = 'default';
  $view->base_table = 'purecloud_conversation';
  $view->human_name = 'Purecloud Conversations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'PureCloud Conversations';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access purecloud';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'start' => 'start',
    'outbound' => 'outbound',
    'name_1' => 'name_1',
    'customer' => 'customer',
    'duration' => 'duration',
    'interact' => 'interact',
    'hold' => 'hold',
    'transfer' => 'transfer',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'start' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'outbound' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'customer' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'duration' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'interact' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hold' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'transfer' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: PureCloud Conversation: Agent name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'purecloud_agent';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: PureCloud Conversation: Call date */
  $handler->display->display_options['fields']['start']['id'] = 'start';
  $handler->display->display_options['fields']['start']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['start']['field'] = 'start';
  $handler->display->display_options['fields']['start']['date_format'] = 'short';
  $handler->display->display_options['fields']['start']['second_date_format'] = 'long';
  /* Field: PureCloud Conversation: Outbound */
  $handler->display->display_options['fields']['outbound']['id'] = 'outbound';
  $handler->display->display_options['fields']['outbound']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['outbound']['field'] = 'outbound';
  $handler->display->display_options['fields']['outbound']['label'] = 'Call direction';
  $handler->display->display_options['fields']['outbound']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['outbound']['alter']['text'] = 'Outbound';
  $handler->display->display_options['fields']['outbound']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['outbound']['empty'] = 'Inbound';
  $handler->display->display_options['fields']['outbound']['empty_zero'] = TRUE;
  /* Field: PureCloud Conversation: Queue name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'purecloud_queue';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
  /* Field: PureCloud Conversation: Customer */
  $handler->display->display_options['fields']['customer']['id'] = 'customer';
  $handler->display->display_options['fields']['customer']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['customer']['field'] = 'customer';
  $handler->display->display_options['fields']['customer']['label'] = 'Remote party';
  $handler->display->display_options['fields']['customer']['element_label_colon'] = FALSE;
  /* Field: PureCloud Conversation: Total call duration */
  $handler->display->display_options['fields']['duration']['id'] = 'duration';
  $handler->display->display_options['fields']['duration']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['duration']['field'] = 'duration';
  $handler->display->display_options['fields']['duration']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['duration']['granularity'] = '2';
  /* Field: PureCloud Conversation: Agent interaction duration */
  $handler->display->display_options['fields']['interact']['id'] = 'interact';
  $handler->display->display_options['fields']['interact']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['interact']['field'] = 'interact';
  $handler->display->display_options['fields']['interact']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['interact']['granularity'] = '2';
  /* Field: PureCloud Conversation: Agent hold time */
  $handler->display->display_options['fields']['hold']['id'] = 'hold';
  $handler->display->display_options['fields']['hold']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['hold']['field'] = 'hold';
  $handler->display->display_options['fields']['hold']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hold']['granularity'] = '2';
  /* Field: PureCloud Conversation: Transfer */
  $handler->display->display_options['fields']['transfer']['id'] = 'transfer';
  $handler->display->display_options['fields']['transfer']['table'] = 'purecloud_conversation';
  $handler->display->display_options['fields']['transfer']['field'] = 'transfer';
  $handler->display->display_options['fields']['transfer']['element_label_colon'] = FALSE;
  /* Filter criterion: PureCloud Conversation: Agent name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'purecloud_agent';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Agent name';
  $handler->display->display_options['filters']['name']['expose']['description'] = 'Filter by agent name.';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
  );
  /* Filter criterion: PureCloud Conversation: Queue name */
  $handler->display->display_options['filters']['name_1']['id'] = 'name_1';
  $handler->display->display_options['filters']['name_1']['table'] = 'purecloud_queue';
  $handler->display->display_options['filters']['name_1']['field'] = 'name';
  $handler->display->display_options['filters']['name_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name_1']['expose']['operator_id'] = 'name_1_op';
  $handler->display->display_options['filters']['name_1']['expose']['label'] = 'Queue name';
  $handler->display->display_options['filters']['name_1']['expose']['description'] = 'Filter by queue name.';
  $handler->display->display_options['filters']['name_1']['expose']['operator'] = 'name_1_op';
  $handler->display->display_options['filters']['name_1']['expose']['identifier'] = 'name_1';
  $handler->display->display_options['filters']['name_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
  );
  /* Filter criterion: Date: Date (purecloud_conversation) */
  $handler->display->display_options['filters']['date_filter']['id'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['table'] = 'purecloud_conversation';
  $handler->display->display_options['filters']['date_filter']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['operator'] = 'between';
  $handler->display->display_options['filters']['date_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter']['expose']['operator_id'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['label'] = 'Call Date';
  $handler->display->display_options['filters']['date_filter']['expose']['operator'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['identifier'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
  );
  $handler->display->display_options['filters']['date_filter']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter']['date_fields'] = array(
    'purecloud_conversation.start' => 'purecloud_conversation.start',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/purecloud';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'PureCloud';
  $handler->display->display_options['menu']['description'] = 'Filterable and exportable conversations report of data pulled in from the PureCloud API.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/content/purecloud/export';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $views['purecloud_conversations'] = $view;
  return $views;
}

