PureCloud module

PureCloud provides an excellent API by which to access data on support interactions between agents and customers. They currently don't provide any way by which to query said data via an admin account however.

The purecloud module provides a PHP SDK for interacting with the PureCloud API. It also provides drush and cron
wrappers for downloading a subset of the reports as Drupal entities. They can then be filtered, browsed and exported via a views page that the module provides.

An expiry is set in Drupal so that records don't remain in the database indefinitely.

This module was generously sponsored by Quicken.