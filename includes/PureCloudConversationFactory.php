<?php
/**
 * Contains Drupal\purecloud\PureCloudConversationFactory.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudConversationFactory.
 * Mostly procedural process logic.
 */
class PureCloudConversationFactory {

  /**
   * Fetches all pages of conversations for a given time range.
   *
   * @param array $settings
   * PureCloud API settings.
   * @param string $start
   * A PHP Date string for start time, see PHP DateTime for allowed formats.
   * @param string $end
   * A PHP Date string for end time, see PHP DateTime for allowed formats.
   *
   * @return array
   * The created entities.
   */
  public static function conversation(array $settings, $start, $end) {
    if (!strtotime($start) || !strtotime($end)) {
      throw new \InvalidArgumentException('Start and end dates could not be parsed.');
    }
    $analytics_service = PureCloudAPIFactory::fetchAPI($settings, 'analytics');
    $user_service = PureCloudAPIFactory::fetchAPI($settings, 'users');
    $routing_service = PureCloudAPIFactory::fetchAPI($settings, 'routing');
    $start_date = new \DateTime($start);
    $end_date = new \DateTime($end);
    $analytics_service->route('/conversations/details/query')
      ->setInterval($start_date, $end_date);

    $handles = array();
    $entities = array();
    $pager_iterator = new PureCloudAPIPagerIterator($analytics_service);
    $entity_info = entity_get_info('purecloud_conversation');
    foreach ($pager_iterator as $page) {
      // Break out conversations into individual instances per agent.
      $conversation_iterator = new PureCloudAPIConversationIterator($page->conversations);
      // Remove conversations w/o agents from the results.
      $filtered_conversations = new PureCloudAPIConversationFilterIterator($conversation_iterator);
      // Import the entities.
      foreach ($filtered_conversations as $data) {
        $handle = new PureCloudAPIConversationHandle($data);
        $values = array();
        foreach ($entity_info['schema_fields_sql']['base table'] as $property) {
          if (in_array($property, array('id', 'data'))) {
            continue;
          }
          $values[$property] = $handle->get($property);

        }
        // We don't want to save any records without queue and agent attached.
        if (empty($values['queue']) || empty($values['agent'])) {
          continue;
        }
        $entity = entity_create('purecloud_conversation', $values);
        entity_save('purecloud_conversation', $entity);
        self::agent($user_service, $entity->agent);
        self::queue($routing_service, $entity->queue);
        $entities[] = $entity;
      }
    }
    return $entities;
  }

  /**
   * Fetch and update an agent by ID.
   *
   * @param PureCloudAPIUsersQuery $query
   * A PureCloudAPIUsersQuery object to use.
   * @param string $agent_id
   * The agent ID in PureCloud.
   */
  public static function agent(PureCloudAPIUsersQuery $query, $agent_id) {
    if (empty($agent_id) || !is_string($agent_id)) {
      throw new \InvalidArgumentException('Invalid agent UUID.');
    }
    $entity = entity_load_single('purecloud_agent', $agent_id);
    if (!empty($entity)) {
      return FALSE;
    }
    $agent = $query->route("/{$agent_id}")->get()->execute();
    if (empty($agent->name)) {
      return FALSE;
    }
    $values = array(
      'agent' => $agent_id,
      'name' => $agent->name,
    );
    $entity = entity_create('purecloud_agent', $values);
    entity_save('purecloud_agent', $entity);
    return $entity;
  }

  /**
   * Fetch and update a queue by ID.
   *
   * @param PureCloudAPIRoutingQuery $query
   * A PureCloudAPIRoutingQuery object to use.
   * @param string $queue_id
   * The queue ID in PureCloud.
   *
   * @return mixed
   * The new queue entity on success, FALSE on failure.
   */
  public static function queue(PureCloudAPIRoutingQuery $query, $queue_id) {
    if (empty($queue_id) || !is_string($queue_id)) {
      throw new \InvalidArgumentException('Invalid queue UUID.');
    }
    $entity = entity_load_single('purecloud_queue', $queue_id);
    if (!empty($entity)) {
      return FALSE;
    }
    $queue = $query->route("/queues/{$queue_id}")->get()->execute();
    if (empty($queue->name)) {
      return FALSE;
    }
    $values = array(
      'queue' => $queue_id,
      'name' => $queue->name,
    );
    $entity = entity_create('purecloud_queue', $values);
    entity_save('purecloud_queue', $entity);
    return $entity;
  }

}

