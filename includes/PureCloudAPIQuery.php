<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIQuery.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIPlugin.
 */
abstract class PureCloudAPIQuery {

  /**
   * @var PureCloudAPI $api
   * The PureCloudAPI to make requests with.
   */
  private $api;

  /**
   * @var string $method
   * The HTTP method to use when request() is called.
   */
  private $method;

  /**
   * @var string $route
   * The API route to request when request() is called.
   */
  private $route;

  /**
   * @var integer $page
   * The page of the request.
   */
  private $page;

  /**
   * @var array $data
   * Parameters for for the query.
   */
  protected $data = array();

  /**
   * Class constructor
   *
   * @param PureCloudAPI $api
   * A purecloud API object.
   */
  public function __construct(PureCloudAPI $api) {
    $this->api = $api;
    $this->method = 'POST';
    $this->page = 1;
  }

  /**
   * Return a pager version of the plugin.
   *
   * @return PureCloudAPI $api
   * A purecloud API object.
   */
  public function pager() {
    return new PureCloudAPIPagerIterator($this);
  }
  
  /**
   * Return the subroute.
   *
   * @return string 
   *  The API plugin is the part of the route after the API version e.g 
   * "analytics".
   */
  abstract protected function subroute();

  /**
   * Fetch the full path to the endpoint.
   *
   * @return string 
   *  The full path to the endpoint.
   */
  protected function getFullRoute() {
    return "/{$this->subroute()}{$this->route}";
  }

  /**
   * Set the route.
   *
   * @param string $route
   * The route to query.
   * @return PureCloudAPIPlugin
   * Returns itself so additional methods can be chained.
   */
  public function route($route) {
    if (!is_string($route)) {
      throw new InvalidArgumentException('Route must be a string.');
    }
    $this->route = $route;
    return $this;
  }

  /**
   * Change to a GET request, default is POST.
   *
   * @return PureCloudAPIPlugin
   * Returns itself so additional methods can be chained.
   */
  public function get() {
    $this->method = 'GET';
    return $this;
  }

  /**
   * Set the page.
   *
   * @param integer $page
   * The page number to fetch.
   */
  public function page($page) {
    if (!is_int($page)) {
      throw new InvalidArgumentException('Page must be an integer.');
    }    
    $this->page = $page;
  }

  /**
   * Compile and execute the current request.
   */
  public function execute() {
    $options = $this->options();
    $url = $this->getFullRoute($this->route);
    $response = $this->api->request($url, $options);
    $data = NULL;
    if (!empty($response->data)) {
      $data = json_decode($response->data);
    }
    return $data;
  }

  /**
   * Compile request options. Also allows a developer to get debug output.
   *
   * @return array
   * An array of options like those passed to drupal_http_request().
   */
  private function options() {
    $options = array(
      'method' => $this->method,
    );
    if ($this->method != 'GET') {
      $options['data'] = $this->data;
      $options['data']['paging'] = array(
        'pageNumber' => $this->page,
        'pageSize' => 100,
      );
      $options['data'] = json_encode($options['data']);
    }
    return $options;
  }

}

