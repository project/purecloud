<?php
/**
 * @file
 * Contains PurecloudConversationController.
 */

class PureCloudConversationController extends EntityAPIController implements EntityAPIControllerInterface {

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if (empty($entity->session)) {
      throw new EntityMalformedException('All conversations must have a sessionID.');
    }
    // Upstream uses these values to determine if the record needs an insert or 
    // an update.
    if ($id = $this->exists($entity->session)) {
      $entity->is_new = FALSE;
      $entity->id = $id;
    }
    parent::save($entity, $transaction);
  }

  /**
   * Determines whether an existing session record exists.
   *
   * @param string $session
   * The PureCloud session ID.
   *
   * @return mixed
   * The id of the entity if we already have it, FALSE if we don't.
   */
  private function exists($session) {
    $existing_id = db_query('SELECT id, session FROM {purecloud_conversation} WHERE session = :sessionID', array(':sessionID' => $session))->fetchField();
    return $existing_id;
  }

}