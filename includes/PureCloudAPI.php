<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPI.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPI.
 * Makes API requests.
 */
class PureCloudAPI extends PureCloudAPIRequest {

  /**
   * @var string $url
   * The API url.
   */
  private $url;

  /**
   * @var integer $version
   * The API version.
   */
  private $version;

  /**
   * @var PureCloudAPIAuth $authToken
   * A PureCloudAPIAuth token object.
   */
  private $authToken;

  /**
   * Class constructor
   *
   * @param string $api_url
   *  The URL of the API.
   * @param PureCloudAPIAuth $auth
   * A PureCloudAPIAuth token object.
   * @param integer $api_version
   * A PureCloudAPIAuth token object.
   */
  public function __construct($api_url, PureCloudAPIAuthToken $auth_token, $api_version) {
    if (empty($api_url) || empty($api_version)) {
      throw new \InvalidArgumentException('Missing API url or version.');
    }
    $this->url = $api_url;
    $this->authToken = $auth_token;
    $this->version = $api_version;
  }

  /**
   * Make an HTTP request.
   *
   * @param string $endpoint
   *  A string containing the URI of the endpoint.
   * @param array $options
   *  An array of options. See drupal_http_request().
   *
   * @return object
   *  A response object, see drupal_http_request().
   */
  public function request($endpoint, $options) {
    $url = "{$this->url}/api/v{$this->version}{$endpoint}";
    $options['headers']['Content-Type'] = "application/json";
    $options['headers']['Authorization'] = "Bearer {$this->authToken->get()}";
    $response = parent::request($url, $options);
    if ($response->code != 401) {
      return $response;
    }
    $this->authToken->refresh();
    return $this->request($url, $options);
  }

}

