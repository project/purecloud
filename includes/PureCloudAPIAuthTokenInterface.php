<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIAuthTokenInterface.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIAuthTokenInterface.
 * Interface for PureCloud API Authorization.
 */
interface PureCloudAPIAuthTokenInterface {

  /**
   * Get the access token.
   */
  public function get();

  /**
   * Fetch an access token from the API.
   */
  public function refresh();

}

