<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIEntityHandle.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIEntityHandle.
 * Generic handle between the PureCloud API and Drupal's Entity API.
 */
class PureCloudAPIEntityHandle implements PureCloudAPIEntityHandleInterface {
	
  /**
   * @var stdClass $raw
   * The raw data of the conversation.
   */
  protected $data;

  /**
   * Class constructor.
   *
   * @param object $data
   * JSON decoded data output from the API for a single item.
   */
  public function __construct(\stdClass $data) {
    $this->data = $data;
  }

  /**
   * Get a value from this object by name.
   *
   * @param string $property
   *  The property to fetch
   *
   * @return string
   *  The value of the property.
   */
  public function get($property) {
    if (empty($property) || !is_string($property) || !method_exists($this, $property)) {
      return NULL;
    }
    return $this->{$property}();
  }

}

