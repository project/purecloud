<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIPagerIterator.php
 */
namespace Drupal\purecloud;

class PureCloudAPIPagerIterator implements \Iterator {

  /**
   * Class constructor.
   *
   * @param PureCloudAPIPlugin $plugin
   *  A PureCloudAPIPlugin plugin.
   * 
   */
  public function __construct(PureCloudAPIQuery $plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Return the current page.
   *
   * @return array
   * The current page.
   */
  public function current() {
    return $this->current;
  }

  /**
   * Return the current page number.
   *
   * @return integer
   * The current page number.
   */
  public function key() {
    return $this->page;
  }

  /**
   * Move forward to next page.
   */
  public function next() {
    $this->page++;
    $this->plugin->page($this->page);
  }

  /**
   * Rewind the Iterator to the first page.
   */
  public function rewind() {
    $this->page = 1;
    $this->plugin->page(1);
  }

  /**
   * Checks if current position is valid. If there is no data then we are at the
   * end.
   *
   * @return boolean
   * TRUE is there is data FALSE if not.
   */ 
  public function valid() {
    $data = $this->plugin->execute();
    if (empty((array) $data)) {
      return FALSE;
    }
    $this->current = $data;
    return TRUE;
  }

}

