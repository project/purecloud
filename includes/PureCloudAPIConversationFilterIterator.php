<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIConversationFilterIterator.
 */
namespace Drupal\purecloud;

class PureCloudAPIConversationFilterIterator extends \RecursiveFilterIterator {

  /**
   * Check whether the current conversation has an agent.
   */
  public function accept() {
    $current = parent::current();
    return !empty($current->agent) && !empty($current->customer);
  }

}

