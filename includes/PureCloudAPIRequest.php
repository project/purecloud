<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIRequest.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIRequest.
 * Makes http requests.
 */
abstract class PureCloudAPIRequest {

  /**
   * Make an HTTP request.
   *
   * @param string $url
   *  A string containing a fully qualified URI.
   * @param array $options
   *  An array of options. See drupal_http_request().
   *
   * @return object
   *  A response object, see drupal_http_request().
   */
  protected function request($url, $options) {
    $response = drupal_http_request($url, $options);
    // 200 and 401 are the only codes that come back from the PureCloud API that
    // don't in some way indicate an application error. 401 can be a token
    // expiration.
    if (!in_array($response->code, array(200, 401))) {
      watchdog('PureCloud', json_encode($response));
    }
    return $response;
  }

}

