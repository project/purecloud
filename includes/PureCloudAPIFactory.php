<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIFactory
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIFactory
 */
class PureCloudAPIFactory {

  /**
   * Fetch an API Controller.
   *
   * @param array $settings
   * API settings including 
   *  - oauth_endpoint
   *  - client_id
   *  - client_secret
   *  - api_url
   * @param string $plugin
   *  A specific plugin to load called by name. If left blank only the core API 
   *  will load.
   *
   * @return 
   */
  public static function fetchAPI(array $settings, $plugin = NULL) {
    if (!empty($plugin) && !is_string($plugin)) {
      throw new InvalidArgumentException('Plugin name must be ommited or left a string.');
    }
    $auth_token = new PureCloudAPIAuthToken($settings);
    $api = new PureCloudAPI($settings['api_url'], $auth_token, 2);
    switch ($plugin) {
      case 'analytics':
        $api = new PureCloudAPIAnalyticsQuery($api);
        break;
      case 'users':
        $api = new PureCloudAPIUsersQuery($api);
        break;
      case 'routing':
        $api = new PureCloudAPIRoutingQuery($api);
        break;
    }
    return $api;
  }

}

