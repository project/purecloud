<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIEntityHandleInterface.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIEntityHandleInterface.
 * Interface for PureCloud API Entity Handle.
 */
interface PureCloudAPIEntityHandleInterface {

  /**
   * Class constructor.
   *
   * @param object $data
   * JSON decoded data output from the API for a single item.
   */
  public function __construct(\stdClass $data);

  /**
   * Get a value from this object by name.
   *
   * @param string $property
   *  The property to fetch
   *
   * @return string
   *  The value of the property.
   */
  public function get($property);

}

