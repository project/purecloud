<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIAuthToken.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIAuthToken.
 * Fetches and stores the API access token.
 */
class PureCloudAPIAuthToken extends PureCloudAPIRequest implements PureCloudAPIAuthTokenInterface {

  /**
   * @var $endpoint
   * The endpoint for obtaining an access token from the PureCloud API.
   */
  private $endpoint;

  /**
   * @var string $clientID 
   * The oauth client id for this application.
   */
  private $clientID;

  /**
   * @var string $clientSecret
   * The oauth client secret for this application.
   */
  private $clientSecret;

  /**
   * @var string $token
   * The access token for this application.
   */
  private $token;

  /**
   * Class constructor
   *
   * @param array $settings
   * An array of settings containing client_id, client_secret and 
   * oauth_endpoint.
   */
  public function __construct(array $settings) {
    if (empty($settings['client_id']) || empty($settings['client_secret']) || empty($settings['oauth_endpoint'])) {
      throw new \InvalidArgumentException('Missing required API credentials.');
    }
    $this->clientID = $settings['client_id'];
    $this->clientSecret = $settings['client_secret'];
    $this->endpoint = $settings['oauth_endpoint'];
    $this->refresh();
  }

  /**
   * Get the access token.
   */
  public function get() {
    return $this->token;
  }

  /**
   * Fetch a new access token from the API.
   */
  public function refresh() {
    $auth_string = base64_encode("{$this->clientID}:{$this->clientSecret}");
    $query_data = array(
      'grant_type' => 'client_credentials',
    );
    $options = array(
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => "Basic {$auth_string}",
      ),
      'method' => 'POST',
      'data' => http_build_query($query_data),
    );
    $response = $this->request($this->endpoint, $options);
    $auth_data = json_decode($response->data);
    if ($response->code != 200 || !is_object($auth_data) || empty($auth_data->access_token)) {
      throw new RuntimeException('PureCloud Token request failed.');
    }
    $this->token = $auth_data->access_token;
  }

}

