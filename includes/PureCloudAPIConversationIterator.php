<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIConversationIterator.
 */
namespace Drupal\purecloud;

class PureCloudAPIConversationIterator extends \RecursiveArrayIterator implements \RecursiveIterator {

  /**
   * Return current array entry.
   */
  public function current() {
    $current = parent::current();
    foreach ($current->participants as $participant) {
      if (empty($participant->purpose)) {
        continue;
      }
      $current->{$participant->purpose}[] = $participant;
    }
    $current->participants = array();
    return $current;
  }

  /**
   * Returns an iterator for the current conversation.
   */
  public function getChildren() {
    $current = $this->current();
    $children = array();
    foreach ($current->agent as $key => $value) {
      $children[$key] = $current;
      $children[$key]->agent = array($value);
    }
    return new PureCloudAPIConversationIterator($children);
  }

  /**
   * Returns whether current conversation has more than one agent.
   */
  public function hasChildren() {
    $current = $this->current();
    return !empty($current->agent) && count($current->agent) > 1;
  }

}

