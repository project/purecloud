<?php
/**
 * @file
 * Drush comannds for the PureCloud module.
 */

/**
 * Implements hook_drush_command().
 */
function purecloud_drush_command() {
  $items['pc-import'] = array(
    'aliases' => array('pci'),
    'description' => 'Imports a query of PureCloud conversations into Drupal as purecloud_conversation entities.',
    'options' => array(
      'start' => array(
        'description' => 'Start date for the import. Can be any date understood by strtotime().',
        'example-value' => '2016-12-04 10:00:00',
        'value' => 'required',
        'required' => TRUE,
      ),
      'end' => array(
        'description' => 'End date for the import. Can be any date understood by strtotime().',
        'example-value' => '2016-12-04 10:00:00',
        'value' => 'required',
        'required' => TRUE,
      ),
    ),
    'examples' => array(
      "drush pc-import --start='2016-12-04 10:00:00' --end='2016-12-04 12:00:00'" => 'Import all conversations starting on Dec. 4, 2016 at 10:00 and ending on Dec. 4, 2016 at 12:00.',
      "drush pci --start='12/2' --end='12/4'" => 'Import all conversations starting on Dec. 2, 2016 and ending on Dec. 4, 2016.',
    )
  );
  return $items;
}

/**
 * Callback for pc-import.
 */
function drush_purecloud_pc_import() {
  $start = drush_get_option('start');
  $end = drush_get_option('end');

  module_load_include('inc', 'purecloud');
  $records = purecloud_import_conversation_range($start, $end);
  drush_log(sprintf('Success! %d conversations updated!', count($records)), 'success');
}

