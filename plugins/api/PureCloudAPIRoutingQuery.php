<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIRoutingQuery.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIRoutingQuery.
 * Users resource for PureCloud.
 */
class PureCloudAPIRoutingQuery extends PureCloudAPIQuery {

  /**
   * Return the subroute.
   *
   * @return string 
   *  The API plugin is the part of the route after the API version e.g 
   * "analytics".
   */
  protected function subroute() {
    return 'routing';
  }

}

