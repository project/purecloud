<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIUsersQuery.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIUsersQuery.
 * Users resource for PureCloud.
 */
class PureCloudAPIUsersQuery extends PureCloudAPIQuery {

  /**
   * Return the subroute.
   *
   * @return string 
   *  The API plugin is the part of the route after the API version e.g 
   * "analytics".
   */
  protected function subroute() {
    return 'users';
  }

}

