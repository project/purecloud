<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIAnalyticsQuery.
 */
namespace Drupal\purecloud;

/**
 * Drupal\purecloud\PureCloudAPIAnalytics.
 * Analytics resource for PureCloud.
 */
class PureCloudAPIAnalyticsQuery extends PureCloudAPIQuery {

  /**
   * Set the start date for the analytics query.
   *
   * @param DateTime $date
   * A date to set start to.
   *
   * @return PureCloudAPIAnalytics
   * Returns itself so additional methods can be chained.
   */
  public function setInterval(\DateTime $start, \DateTime $end) {
    $this->data['interval'] = "{$start->format(DATE_ATOM)}/{$end->format(DATE_ATOM)}";
    return $this;
  }

  /**
   * Return the subroute.
   *
   * @return string 
   *  The API plugin is the part of the route after the API version e.g 
   * "analytics".
   */
  protected function subroute() {
    return 'analytics';
  }

}

