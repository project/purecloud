<?php
/**
 * @file
 * Contains Drupal\purecloud\PureCloudAPIConversationHandle.
 */
namespace Drupal\purecloud;

use Drupal\purecloud\PureCloudAPIEntityHandle;

/**
 * Drupal\purecloud\PureCloudConversationHandle.
 * Handle between the PureCloud API and Drupal's entity API.
 */
class PureCloudAPIConversationHandle extends PureCloudAPIEntityHandle {

  /**
   * @var stdClass $agent
   * The agent object as decoded from the API. 
   */
  private $agent;

  /**
   * @var stdClass $customer
   * The customer object as decoded from the API. 
   */
  private $customer;

  /**
   * Class constructor.
   *
   * @param object $data
   * JSON decoded data output from the API for a single item.
   */
  public function __construct(\stdClass $data) {
    parent::__construct($data);
    $this->agent = reset($this->data->agent);
    $this->agent->session = reset($this->agent->sessions);
    $this->customer = reset($this->data->customer);
    $this->calculateCallTimes();
  }

  /**
   * Fetch the session ID from the conversation record.
   *
   * @return string
   * 
   */
  protected function session() {
    return $this->agent->session->sessionId;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function agent() {
    return $this->agent->userId;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function start() {
    $start = new \DateTime($this->data->conversationStart);
    return $start->getTimestamp();
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function outbound() {
    if (empty($this->agent->session->direction) || $this->agent->session->direction != 'outbound') {
      return 0;
    }
    return 1;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function queue() {
    $segment = reset($this->agent->session->segments);
    if (empty($segment->queueId)) {
      return NULL;
    }
    return $segment->queueId;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function customer() {
    if (empty($this->customer->participantName)) {
      return 'n/a';
    }
    return $this->customer->participantName;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function duration() {
    return $this->data->duration;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function interact() {
    return $this->agent->duration;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function hold() {
    return $this->data->hold;
  }

  /**
   * Fetch the agent from the conversation record.
   *
   * @return string
   * 
   */
  protected function transfer() {
    return $this->data->transfer;
  }

  /**
   * Utility to add session metric times.
   */
  private function calculateCallTimes() {
    $this->data->duration = 0;
    $this->agent->duration = 0;
    $this->data->hold = 0;
    $this->data->transfer = 0;
    foreach ($this->agent->session->segments as $segment) {
      if (empty($segment->segmentStart) || empty($segment->segmentEnd)) {
        continue;
      }
      $start = new \DateTime($segment->segmentStart);
      $end = new \DateTime($segment->segmentEnd);
      $segment_duration = $end->getTimestamp() - $start->getTimestamp();
      $this->data->duration += $segment_duration;
      switch ($segment->segmentType) {
        case 'interact':
          $this->agent->duration += $segment_duration;
          break;
        case 'hold':
          $this->data->hold += $segment_duration;
          break;
      }
      if (!empty($segment->disconnectType) && $segment->disconnectType == 'transfer') {
        $this->data->transfer = 1;
      }
    }
  }

}

